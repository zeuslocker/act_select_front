import React from 'react';
import MainSlider from './MainSlider'
import MainNavBar from './MainNavBar'
import FlowPresent from './FlowPresent'
import styles from './index.module.scss'
import { connect } from 'react-redux'
import types from 'constants/actionTypes'
import PropTypes from 'prop-types'
import { isAuthenticated } from 'utils/auth'
import { Redirect } from 'react-router-dom'
import modals from 'constants/modals'

class Home extends React.Component {
  constructor(props) {
    super(props)
    this.handleLoginClick = this.handleLoginClick.bind(this)
  }

  handleLoginClick() {
    this.props.dispatch({
      type: types.SET_CURRENT_MODAL,
      payload: { currentModal: modals.AuthModal }
    })
  }

  render() {
    if(isAuthenticated()) return <Redirect to='/profile'/>

    return (
    <>
      <div className={styles.slider_wrapper}>
        <MainNavBar handleLoginClick={this.handleLoginClick}/>
        <MainSlider/>
      </div>
      <div className="my-2">
        <FlowPresent/>
      </div>
    </>
    )
  }
}

Home.propTypes = {
  dispatch: PropTypes.func.isRequired
}

export default connect()(Home)
