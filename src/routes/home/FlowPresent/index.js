import React, { Component } from 'react'
import { Container, Row } from 'reactstrap'
import styles from './index.module.scss'

class FlowPresent extends Component {
  render() {
    return (
      <Container fluid>
        <Row className="justify-content-around">
          <div className="col-auto">
            <img alt="" className={styles.flow_img1} src="/images/flow_present/iphone_x.png"/>
          </div>
          <div className={`col-auto ${styles.arrow_container}`}>
            <img alt="" className={styles.arrow} src="/images/flow_present/arrow.png"/>
          </div>
          <div className="col-auto">
            <img alt="" className={styles.flow_img1} src="/images/flow_present/iphone_x.png"/>
          </div>
          <div className={`col-auto ${styles.arrow_container}`}>
            <img alt="" className={styles.arrow} src="/images/flow_present/arrow.png"/>
          </div>
          <div className="col-auto">
            <img alt="" className={styles.flow_img1} src="/images/flow_present/iphone_x.png"/>
          </div>
        </Row>
      </Container>
    )
  }
}

export default FlowPresent
