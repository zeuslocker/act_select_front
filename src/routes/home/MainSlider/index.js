import React, { PureComponent } from 'react';
import Swiper from 'react-id-swiper';
import styles from './index.module.scss'

class MainSlider extends PureComponent {
  render() {
    const params = {
      noSwiping: true,
      effect: "fade",
      autoplay: {
        delay: 5000
      }
    }

    return (
        <Swiper {...params} containerClass={styles.container}>
          <h1 className={styles.title}>Select Action Find People Act!</h1>
          <div className={styles.img1}/>
          <div className={styles.img2}/>
          <div className={styles.img3}/>
          <div className={styles.img4}/>
      </Swiper>
    )
  }
}

export default MainSlider
