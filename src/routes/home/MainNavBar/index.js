import React, { PureComponent } from 'react'
import styles from './index.module.scss'
import PropTypes from 'prop-types'

class MainNavBar extends PureComponent {
  render() {
    const { handleLoginClick } = this.props

    return (
      <div className={styles.container}>
        <nav className="navbar navbar-light justify-content-between">
          <a className="navbar-brand" href="/">Navbar</a>
          <button className={`mr-4 ${styles.btn_login}`} onClick={handleLoginClick}>Login</button>
        </nav>
      </div>
    );
  }
}

MainNavBar.propTypes = {
  handleLoginClick: PropTypes.func.isRequired
}

export default MainNavBar
