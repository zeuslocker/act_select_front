import React, { Component } from 'react'
import { Router as ReactRouter, Route } from 'react-router-dom'
import { history } from 'utils/history'
import Home from './home'
import Profile from './profile'

class Router extends Component {
  render() {
    return (
      <ReactRouter history={history}>
        <>
          <Route path="/" exact component={Home}/>
          <Route path="/profile" exact component={Profile}/>
        </>
      </ReactRouter>
    )
  }
}

export default Router
