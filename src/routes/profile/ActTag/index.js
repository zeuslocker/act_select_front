import React, { PureComponent } from 'react'
import styles from './index.module.scss'
import { graphql } from 'react-apollo'
import { loader } from 'graphql.macro'

const TOGGLE_SUBSCRIBE_ACT = loader('../../../apollo/mutations/toggleSubscribeAct.graphql')
const ME_ACTS = loader('../../../apollo/queries/me_acts.graphql')

class ActTag extends PureComponent {
  render() {
    const { act, toggleSubscribeAct, subscribed } = this.props

    return (
        <div className={styles.act_cell}>
          <span className={styles.act_tag}>
            {act.name}
          </span>
          <span className={styles.subscribers_multiplier}>
            <span className={styles.multiplier}>
              {'×'}
              {act.subscribersCount}
            </span>
          </span>
            <div className={styles.description}>
              {act.description}
            </div>
          <div className='row no-gutters justify-content-between'>
            <div className={styles.matches}> 100 matches this week
            </div>
            {subscribed ?
            <button
              type="button"
              className={`btn btn-outline-dark ${styles.btn_unsubscribe}`}
              onClick={toggleSubscribeAct}
            >
              unsubscribe
            </button>
            :
            <button
              type="button"
              className={`btn btn-outline-dark ${styles.btn_subscribe}`}
              onClick={toggleSubscribeAct}
            >
              subscribe
            </button>
            }
          </div>
        </div>
    )
  }
}

export default graphql(TOGGLE_SUBSCRIBE_ACT,
  {
    name: 'toggleSubscribeAct',
    options: (props) => ({
      variables: {
        actId: props.act.id
      },
      update: (proxy, info) => {
        const data = proxy.readQuery({ query: ME_ACTS })

        if(info.data.toggleSubscribeAct.subscribed) {
          data.me.acts.push(props.act)
        } else {
          data.me.acts = data.me.acts.filter((act) =>(act.id !== props.act.id))
        }
        proxy.writeQuery({ query: ME_ACTS, data })
      }
})})(ActTag)
