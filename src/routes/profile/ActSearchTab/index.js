import React, { PureComponent } from 'react'
import ActTag from '../ActTag'
import styles from './index.module.scss'

class ActSearchTab extends PureComponent {
  render() {
    const { acts, meActs } = this.props
    return (
      <div className={styles.acts_container}>
        <div className="row justify-content-between mt-4 mb-3 mx-4 w-100">
          <div className="form-group col-auto">
            <input
              type="text"
              className="form-control"
              id="formGroupExampleInput"
              placeholder="Find by act name"
            />
          </div>
          <div className="col-auto">
            <a href='#' className={styles.left_sort}>
              Popular
            </a>
            <a href='#' className={styles.middle_sort}>
              Name
            </a>
            <a href='#' className={styles.right_sort}>
              New
            </a>
          </div>
        </div>
        <div className={styles.acts_browser}>
          {acts.map((act) => <ActTag
            key={act.id}
            act={act}
            subscribed={meActs.find((meAct) =>(meAct.id === act.id))}
          />)}
        </div>
      </div>
    )
  }
}

export default ActSearchTab
