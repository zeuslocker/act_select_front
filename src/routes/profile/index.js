import React from 'react'
import ProfileView from './ProfileView.js'
import { connect } from 'react-redux'
import { Query } from 'react-apollo'
import { loader } from 'graphql.macro'
import { isAuthenticated } from 'utils/auth'
import { Redirect } from 'react-router-dom'
import types from 'constants/actionTypes'
import modals from 'constants/modals'

const ME_QUERY = loader('../../apollo/queries/me.graphql')

class Profile extends React.Component {
  constructor(props) {
    super(props)
    this.handleCreateAct = this.handleCreateAct.bind(this)
  }

  handleCreateAct() {
    this.props.dispatch({
      type: types.SET_CURRENT_MODAL,
      payload: { currentModal: modals.CreateActModal }
    })
  }

  render() {
    if(isAuthenticated() === false) return <Redirect to='/'/>

    return (
      <Query query={ME_QUERY}>
        {(queryState) =>(<ProfileView {...queryState} onCreateAct={this.handleCreateAct}/>)}
      </Query>
    )
  }
}

export default connect()(Profile)
