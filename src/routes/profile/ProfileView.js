import React, { PureComponent } from 'react'
import { MainLoader } from 'components/loaders'
import PropTypes from 'prop-types'
import styles from './index.module.scss'
import Avatar from 'components/Avatar'
import { Tabs, TabLink, TabContent } from 'react-tabs-redux'
import ProfileTab from './ProfileTab'
import ActSearchTab from './ActSearchTab'

class ProfileView extends PureComponent {
  render() {
    const { loading, error, data } = this.props
    if (loading) return <MainLoader fullScreen />
    if (error) return null

    const src = process.env.REACT_APP_API_BASE_URL + data.me.photos[0].image
    const acts = data.actList
    const meActs = data.me.acts

    return (
      <div className={`container-fluid p-0 ${styles.container}`}>
        <Tabs className='h-100'>
          <div className="row no-gutters h-100">
            <div className={`col-md-3 d-none d-md-block ${styles.profile_menu}`}>
              <div className={`${styles.nav_bar} mb-3`}>
                <Avatar src={src} type="circle" size="thumb" className={styles.avatar}/>
                <p className={styles.profile_title}>My Profile</p>
              </div>
              <TabLink
                to='ProfileTab'
                className={styles.tab_link}
                activeClassName={styles.active_tab_link}
              >
                Profile
              </TabLink>
              <TabLink
                to='ActSearchTab'
                className={styles.tab_link}
                activeClassName={styles.active_tab_link}
              >
                Acts
              </TabLink>
            </div>
            <div className="col-md-9 col-sm-12">
              <TabContent for='ProfileTab'>
                <ProfileTab/>
              </TabContent>
              <TabContent for='ActSearchTab'>
                <ActSearchTab meActs={meActs} acts={acts}/>
              </TabContent>
            </div>
          </div>
        </Tabs>
      </div>
    )
  }
}

ProfileView.propTypes = {
  loading: PropTypes.bool.isRequired,
  error: PropTypes.object,
  data: PropTypes.object
}

export default ProfileView
