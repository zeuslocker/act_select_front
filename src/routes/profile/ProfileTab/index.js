import React, { PureComponent, useCallback } from 'react'
import { Form, Field } from 'react-final-form'
import { useDropzone } from 'react-dropzone'
import client from 'utils/apollo'
import { loader } from 'graphql.macro'
const CREATE_ACT_MUTATION = loader('../../../apollo/mutations/createAct.graphql')

function MyDropzone({input}) {
  const onDrop = useCallback(acceptedFiles => {
    input.onChange(acceptedFiles)
  }, [])
  const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})

  return (
    <div {...getRootProps()}>
      <input {...getInputProps()} />
      {
        isDragActive ?
          <p>Drop the files here ...</p> :
          <p>Drag 'n' drop some files here, or click to select files</p>
      }
    </div>
  )
}

class ProfileTab extends PureComponent {
  onSubmit = (values) => {
    debugger
    client.mutate({mutation: CREATE_ACT_MUTATION, variables: values}).then((res) => {
      debugger
    })
  }
  render() {

    return (
      <Form
        onSubmit={this.onSubmit}
        render={({handleSubmit}) => (
          <form onSubmit={handleSubmit}>
            <div>
              <label>Name</label>
              <Field name='name' component='input' placeholder='Name'/>
            </div>
            <div>
              <label>Description</label>
              <Field name='description' component='input' placeholder='Description'/>
            </div>
            <div>
              <label>Photos</label>
              <Field name='photos' component={MyDropzone} placeholder='Photos'/>
            </div>
            <button type='submit'>Submit</button>
          </form>
        )}
      />
    )
  }
}

export default ProfileTab
