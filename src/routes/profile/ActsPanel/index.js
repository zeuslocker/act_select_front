import React, { PureComponent } from 'react'
import ActTag from '../ActTag'
import styles from './index.module.scss'

class ActsPanel extends PureComponent {
  render() {
    const { acts } = this.props

    return (
      <div className={styles.acts_panel}>
        {acts.map((act) => (<ActTag act={act}/>))}
      </div>
    )
  }
}

export default ActsPanel
