import React, { Component } from 'react'
import PropTypes from 'prop-types'

class FacebookLogin extends Component {
  /**
   * Check login status and call login api is user is not logged in
   */
  facebookLogin = () => {
    if (!window.FB) return;

    window.FB.getLoginStatus(response => {
      if (response.status === 'connected') {
        this.facebookLoginHandler(response);
      } else {
        window.FB.login(this.facebookLoginHandler, {scope: 'user_gender,user_birthday,user_photos,email,user_friends,user_likes'});
      }
    }, true)
  }

  /**
   * Handle login response
   */
  facebookLoginHandler = response => {
    if (response.status === 'connected') {
      this.props.onLogin(true, response.authResponse.accessToken);
    } else {
      this.props.onLogin(false);
    }
  }

  render() {
    const { children } = this.props;

    const childrenWithProps = React.Children.map(children, child =>
      React.cloneElement(child, { onClick: this.facebookLogin })
    )

    return <>{childrenWithProps}</>
  }
}

FacebookLogin.propTypes = {
  onLogin: PropTypes.func.isRequired,
  children: PropTypes.node
}

export default FacebookLogin
