import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'ramda'
import { AuthModal, ErrorModal, CreateActModal } from 'components/modals'
import types from 'constants/actionTypes'
import modals from 'constants/modals'
import PropTypes from 'prop-types'

class Modal extends Component {
  constructor(props) {
    super(props)
    this.close = this.close.bind(this)
    this.handleFacebookLogin = this.handleFacebookLogin.bind(this)
  }

  // CALLBACKS FOR ALL MODALS START

  close() {
    this.props.dispatch({type: types.SET_CURRENT_MODAL, payload: {
      currentModal: null
    }})
  }

  handleFacebookLogin(loginStatus, accessToken) {
    const { dispatch } = this.props

    if(loginStatus === true) {
      dispatch({ type: types.ME_LOGIN_REQUEST, payload: { accessToken } })
    } else {
      //throw {code: 401, message: "Something went wrong"}
    }
  }

  // CALLBACKS FOR ALL MODALS END

  render() {
    switch (this.props.currentModal) {
      case modals.AuthModal:
        return <AuthModal close={this.close} onLogin={this.handleFacebookLogin}/>
      case modals.ErrorModal:
        return <ErrorModal close={this.close}/>
      case modals.CreateActModal:
        return <CreateActModal close={this.close}/>
      default:
        return null
    }
  }
}

Modal.propTypes = {
  currentModal: PropTypes.string,
  dispatch: PropTypes.func.isRequired
}

export default compose(connect(
  (state) => ({ currentModal: state.modal.currentModal })
))(Modal)
