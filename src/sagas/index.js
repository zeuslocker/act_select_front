import types from 'constants/actionTypes'
import { takeLatest, all } from 'redux-saga/effects'
import * as me from 'sagas/me'
import * as shared from 'sagas/shared'

export default function* rootSaga() {
  yield all([
    yield takeLatest(types.ME_LOGIN_REQUEST, me.login),
    yield takeLatest(types.ME_PROFILE_REQUEST, me.profile),
    yield takeLatest(types.GENERAL_ERROR, shared.generalError)
  ])
}
