import types from 'constants/actionTypes'
import modals from 'constants/modals'
import { put } from 'redux-saga/effects'

export function* generalError({ payload }) {
  console.log(payload.error) // eslint-disable-line no-console
  yield put({ type: types.SET_CURRENT_MODAL, payload: { currentModal: modals.ErrorModal } })
}
