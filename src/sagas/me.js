import { call, put } from 'redux-saga/effects'
import localStorageTools from 'utils/localStorageTools'
import types from 'constants/actionTypes'
import { history } from 'utils/history'
import { path } from 'ramda'
import client from 'utils/apollo'
import { loader } from 'graphql.macro'

const FB_CREATE_SESSION_MUTATION = loader('../apollo/mutations/fbCreateSession.graphql')
const ME_QUERY = loader('../apollo/queries/me.graphql')

export function* login({payload}) {
  const { accessToken } = payload
  const result = yield call(client.mutate, {
    mutation: FB_CREATE_SESSION_MUTATION,
    variables: { fbAccessToken: accessToken }
  })

  if(path(['errors'], result)) {
    yield put({type: types.ME_LOGIN_FAILURE, payload: { errors: result.errors }})
  } else {
    const data = result.data.fbCreateSession
    localStorageTools.setItem('APIAccessToken', data.accessToken)
    localStorageTools.setItem('APIAccessExpiredAt', data.accessExp)
    localStorageTools.setItem('APIRefreshToken', data.refreshToken)
    yield put({ type: types.SET_CURRENT_MODAL, payload: { currentModal: null }})
    yield history.push('/profile')
  }
}


export function* profile() {
  const result = yield call(client.query, { query: ME_QUERY })
  if(path['errors']) {
    yield put({type: types.ME_PROFILE_FAILURE, payload: { errors: result.errors }})
  } else {
    yield put({ type: types.ME_PROFILE_SUCCESS, payload: { me: result.data.me }})
  }
}
