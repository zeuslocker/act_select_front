const asyncActions = [
  'ME_LOGIN',
  'ME_PROFILE'
]

export default asyncActions
