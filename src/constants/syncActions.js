const syncActions = [
  'SET_CURRENT_MODAL',
  'GENERAL_ERROR'
]

export default syncActions
