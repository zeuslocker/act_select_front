import types from 'constants/actionTypes'
import { merge } from 'ramda'

const initialState = {
  currentModal: null
}

export default (state = initialState, action) => {
  const { type } = action

  switch (type) {
    case types.SET_CURRENT_MODAL: {
      return merge(state, { currentModal: action.payload.currentModal })
    }
    default:
      return state
  }
}
