import { combineReducers } from 'redux'
import modal from './modal.js'

export default function createRootReducer() {
  return combineReducers(
    {
      modal
    }
  )
}
