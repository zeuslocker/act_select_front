import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import Auth from 'utils/auth'

export const PrivateRoute = ({ component: Component, unAuthRedirectPath, role, ...rest }) => (
  <Route {...rest} render={(props) => (
    Auth.isAuthenticated(role) === true
      ? <Component {...props} />
      : <Redirect to={unAuthRedirectPath} />
  )} />
)
