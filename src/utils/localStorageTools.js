const namespacedKey = (k) => `ACT_SELECT_WEB/${k}`

export const setItem = (k, v) => (
  localStorage.setItem(namespacedKey(k), JSON.stringify(v))
)

export const getItem = k => {
  const value = localStorage.getItem(namespacedKey(k))

  if (value) {
    return JSON.parse(value)
  }
  return null
}

export const removeItem = k => (
  localStorage.removeItem(namespacedKey(k))
)

export default {
  getItem,
  removeItem,
  setItem
}
