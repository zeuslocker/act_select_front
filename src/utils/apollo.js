import { ApolloLink } from 'apollo-link'
import { ApolloClient } from 'apollo-client';
import RefreshClient from "apollo-boost";
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import localStorageTools from 'utils/localStorageTools'
import { onError } from "apollo-link-error";
import store from 'utils/redux'
import types from 'constants/actionTypes'
import { loader } from 'graphql.macro'
import { createLink } from "apollo-absinthe-upload-link"

const REFRESH_SESSION_MUTATION = loader('../apollo/mutations/refreshSession.graphql')

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if(graphQLErrors) {
    store.dispatch({type: types.GENERAL_ERROR, payload: { error: graphQLErrors }})
  } else {
    store.dispatch({type: types.GENERAL_ERROR, payload: { error: networkError }})
  }
})

const httpLink = createHttpLink({
  uri: process.env.REACT_APP_API_GRAPHQL_URL // eslint-disable-line no-undef
});

const refreshClient = new RefreshClient({
  uri: process.env.REACT_APP_API_GRAPHQL_URL // eslint-disable-line no-undef
})

const absintheUploadLink = createLink({
  uri: process.env.REACT_APP_API_GRAPHQL_URL
})
const refreshCredentials = new Promise((resolve) => {
  if(tokenIsExpired()) {
      const refreshToken = refreshClient.mutate(
        {
          mutation: REFRESH_SESSION_MUTATION,
          variables: { refreshToken: localStorageTools.getItem('APIRefreshToken') }
        }
      )

      refreshToken.then((result) => {
        const data = result.data.refreshSession

        localStorageTools.setItem('APIAccessToken', data.accessToken)
        localStorageTools.setItem('APIRefreshToken', data.refreshToken)
        localStorageTools.setItem('APIAccessExpiredAt', data.accessExp)
      }).then(() => resolve())
    } else {
      resolve()
    }
  })

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  return refreshCredentials.then(() => {
    const token = localStorageTools.getItem('APIAccessToken')
    // return the headers to the context so httpLink can read them
    return {
      headers: {
        ...headers,
        Authorization: token,
      }
    }
  })
})

{/* links order matters */}
const client = new ApolloClient({
  link: ApolloLink.from([authLink, errorLink, absintheUploadLink, httpLink]),
  cache: new InMemoryCache()
})

function tokenIsExpired() {
  // if token ttl less than 1.5 minute
  return localStorageTools.getItem('APIAccessExpiredAt') &&
    (new Date().getTime() / 1000) > localStorageTools.getItem('APIAccessExpiredAt')
}


export default client
