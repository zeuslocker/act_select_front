import localStorageTools from 'utils/localStorageTools'
/* eslint-disable */

const isAuthenticated = () => {
 return typeof(localStorageTools.getItem('APIAccessToken')) === 'string'
}

export {
  isAuthenticated
}
