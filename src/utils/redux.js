import createRootReducer from 'reducers'
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootSaga from 'sagas'

const sagaMiddleware = createSagaMiddleware()
const store = createStore(createRootReducer(), applyMiddleware(sagaMiddleware))

window.store = store
sagaMiddleware.run(rootSaga)

export default store
