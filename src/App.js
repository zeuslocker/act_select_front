import React, { Component } from 'react'
import { Provider } from 'react-redux'
import Router from 'routes'
import Modal from 'containers/Modal'
import client from 'utils/apollo'
import store from 'utils/redux'
import { ApolloProvider } from 'react-apollo'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ApolloProvider client={client}>
          <>
            <Router/>
            <Modal/>
          </>
        </ApolloProvider>
      </Provider>
    )
  }
}

export default App
