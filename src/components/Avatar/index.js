import React, { PureComponent } from 'react'
import styles from './index.module.scss'

class Avatar extends PureComponent {
  render() {
    const { src, type, size, className } = this.props

    return (
      <img src={src} alt="" className={`${styles['type_' + type]} ${styles['size_' + size]} ${className}`}/>
    )
  }
}

export default Avatar
