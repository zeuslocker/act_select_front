import React, { PureComponent } from 'react'
import styles from './index.module.scss'

class OutlinedButton extends PureComponent {
  render() {
    const { onClick, className } = this.props

    return (
      <button className={`${styles.btn_outline} ${className}`} onClick={onClick}>
        {this.props.children}
      </button>
    )
  }
}

export default OutlinedButton
