import React from 'react'
import { ClipLoader } from 'react-spinners'
import styles from './index.module.scss'
import PropTypes from 'prop-types'

 class MainLoader extends React.Component {
  //other logic
    render() {
     const { fullScreen } = this.props

     return(
       <div className={fullScreen ? styles.full_screen_wrapper : styles.wrapper}>
        <ClipLoader
          loading={true}
          color={fullScreen ? 'white' : 'blue'}
        />
       </div>
     );
    }
 }

MainLoader.propTypes = {
  fullScreen: PropTypes.bool
}

export default MainLoader
