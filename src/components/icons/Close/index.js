import React, { memo } from 'react'
import styles from './index.module.scss'

export default memo(() => (
  <div className={`float-right ${styles.wrapper}`}>
  <svg className={styles.icon} width="22" height="22" viewBox="0 0 22 22">
    <path
      d="M14.6 11.7v-1.4l6.5 6.5c1.3 1.2 1.3 3 0 4.3-1 1.3-3 1.3-4.2 0l-6.5-6.4h1.4L5.2 21c-1.2 1.3-3 1.3-4.3 0-1.3-1-1.3-3 0-4.2l6.4-6.5v1.4L1 5.2C-.4 4-.4 2.2 1 1 2-.4 4-.4 5 1l6.5 6.4h-1.4L16.8 1C18-.4 19.8-.4 21 1c1.3 1 1.3 3 0 4.2l-6.4 6.5z"
      stroke="none"
      strokeWidth="1"
    />
  </svg>
  </div>
))
