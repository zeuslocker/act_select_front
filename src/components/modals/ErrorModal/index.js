import React, { PureComponent } from 'react'
import { Container, Modal, ModalBody, Row } from 'reactstrap'
import PropTypes from 'prop-types'
import CloseIcon from 'components/icons/Close'
import styles from './index.module.scss'

class ErrorModal extends PureComponent {
  render() {
    const { close } = this.props

    return (
        <Modal className={styles.modal_container} isOpen={true} toggle={close}>
          <ModalBody>
            <Container fluid className="p-0">
              <Row noGutters className="justify-content-end">
              <div className="col-auto" onClick={close}>
                <CloseIcon/>
              </div>
              </Row>
              <Row className="justify-content-center">
                <div className={`col-auto ${styles.modal_header}`}>
                  Oops something went wrong.
                </div>
              </Row>
            </Container>
          </ModalBody>
        </Modal>
    )
  }
}

ErrorModal.propTypes = {
  close: PropTypes.func.isRequired
}

export default ErrorModal
