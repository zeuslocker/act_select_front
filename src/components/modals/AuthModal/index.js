import React, { PureComponent } from 'react'
import { Container, Modal, ModalBody, Row } from 'reactstrap'
import PropTypes from 'prop-types'
import CloseIcon from 'components/icons/Close'
import FacebookLoginButton from 'containers/FacebookLoginButton'
import styles from './index.module.scss'

class AuthModal extends PureComponent {
  render() {
    const { close, onLogin } = this.props

    return (
        <Modal className={styles.modal_container} isOpen={true} toggle={close}>
          <ModalBody>
            <Container fluid className="p-0">
              <Row noGutters className="justify-content-end">
              <div className="col-auto" onClick={close}>
                <CloseIcon/>
              </div>
              </Row>
              <Row className="justify-content-center">
                <div className={`col-auto ${styles.modal_header}`}>
                  {'let\'s start'}
                </div>
              </Row>
              <Row className="justify-content-center">
                <div className="col-10">
                <FacebookLoginButton onLogin={onLogin}>
                  <button className={styles.fb_login}>log in with facebook</button>
                </FacebookLoginButton>
                </div>
              </Row>
            </Container>
          </ModalBody>
        </Modal>
    )
  }
}

AuthModal.propTypes = {
  close: PropTypes.func.isRequired,
  onLogin: PropTypes.func.isRequired
}

export default AuthModal
