import React, { PureComponent } from 'react'
import CloseIcon from 'components/icons/Close'
import { Container, Modal, ModalBody, Row } from 'reactstrap'
import styles from './index.module.scss'

class CreateActModal extends PureComponent {
  render() {
    const { close } = this.props

    return (
      <Modal className={styles.modal_container} isOpen={true} toggle={close}>
        <ModalBody>
          <Container fluid className="p-0">
            <Row noGutters className="justify-content-end">
              <div className="col-auto" onClick={close}>
                <CloseIcon/>
              </div>
            </Row>
          </Container>
        </ModalBody>
      </Modal>
    );
  }
}

export default CreateActModal
